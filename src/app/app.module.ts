import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";


import {AppComponent} from "./app.component";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {ConsumptionDataService} from "./services/consumption-data.service";
import {HttpModule} from "@angular/http";
import {ConsumptionComponent} from "./consumption/consumption.component";


@NgModule({
  declarations: [
    AppComponent,
    ConsumptionComponent
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    HttpModule
  ],
  providers: [ConsumptionDataService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
